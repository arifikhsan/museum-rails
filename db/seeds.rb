# frozen_string_literal: true

require 'csv'

User.create(
  name: 'Admin',
  email: 'admin@example.com',
  password: '123456',
  is_admin: true
)

User.create(
  name: 'Agus',
  email: 'agus@example.com',
  password: '123456'
)

[
  { name: 'Museum Benda Budaya dan Kesenian', color: '#48bb78'  },
  { name: 'Museum Pendidikan dan Ilmu Pengetahuan', color: '#f56565' },
  { name: 'Museum Sejarah dan Perjuangan', color: '#667eea' }
].each do |category|
  Category.find_or_create_by(category)
end

csv_text = File.read(Rails.root.join('lib', 'seeds', 'museums.csv'))
csv = CSV.parse(csv_text, headers: true)
csv.each do |row|
  museum = row.to_h
  museum.delete_if { |key, value| key.nil? && value.nil? }
  Museum.find_or_create_by(museum) if Museum.new(museum).valid?
end

[
  {
    museum_id: Museum.first.id,
    name: 'Seni Tari',
    detail: 'Adipisicing labore non laboris laborum. Irure excepteur Lorem velit nulla aliqua fugiat ea consectetur anim Lorem. Velit culpa consectetur occaecat irure ipsum veniam nisi magna proident enim laboris mollit reprehenderit occaecat. Ullamco mollit elit reprehenderit irure veniam nostrud cillum laboris officia nisi. Eiusmod pariatur exercitation sint laboris nulla excepteur ex. Non velit cupidatat voluptate adipisicing aliqua id cillum eu amet velit.',
    date_start: '2021-03-15 17:09:22',
    date_end: '2022-03-15 17:09:22',
    price: '2000',
    quota: 10,
  },
  {
    museum_id: Museum.first.id,
    name: 'Pameran Lukis',
    detail: 'Mollit ullamco cupidatat Lorem occaecat elit Lorem mollit sint laborum incididunt eiusmod proident proident id. Aliqua consequat ut pariatur nostrud sunt eu laboris duis ipsum tempor ipsum. Fugiat et id reprehenderit minim in et nostrud.',
    date_start: '2021-03-17 17:09:22',
    date_end: '2022-03-17 17:09:22',
    price: '3000',
    quota: 10,
  },
  {
    museum_id: Museum.second.id,
    name: 'Tari Tradisional',
    detail: 'Sunt nisi nisi do veniam irure adipisicing voluptate dolor proident duis sit ea qui. Sint esse fugiat veniam minim aliqua. Duis aute mollit cillum fugiat aliqua est voluptate labore fugiat in. Consequat reprehenderit aute ad Lorem ex nostrud eu ut culpa cupidatat exercitation occaecat. Laboris labore eu nulla excepteur id amet esse.',
    date_start: '2021-03-16 17:09:22',
    date_end: '2022-03-16 17:09:22',
    price: '4000',
    quota: 10,
  },
  {
    museum_id: Museum.first.id,
    name: 'Pameran Seni Lukis Modern',
    detail: 'Sunt nisi nisi do veniam irure adipisicing voluptate dolor proident duis sit ea qui. Sint esse fugiat veniam minim aliqua. Duis aute mollit cillum fugiat aliqua est voluptate labore fugiat in. Consequat reprehenderit aute ad Lorem ex nostrud eu ut culpa cupidatat exercitation occaecat. Laboris labore eu nulla excepteur id amet esse.',
    date_start: '2021-03-16 17:09:22',
    date_end: '2022-03-16 17:09:22',
    price: '0',
    quota: 10,
  },
  {
    museum_id: Museum.first.id,
    name: 'Pameran Benda Peninggalan Kerajaan Jawa',
    detail: 'Sunt nisi nisi do veniam irure adipisicing voluptate dolor proident duis sit ea qui. Sint esse fugiat veniam minim aliqua. Duis aute mollit cillum fugiat aliqua est voluptate labore fugiat in. Consequat reprehenderit aute ad Lorem ex nostrud eu ut culpa cupidatat exercitation occaecat. Laboris labore eu nulla excepteur id amet esse.',
    date_start: '2020-03-16 17:09:22',
    date_end: '2020-03-16 17:09:22',
    price: '4000',
    quota: 10,
  }
].each do |category|
  Event.find_or_create_by(category)
end

EventUser.create(user_id: User.last.id, event_id: Event.first.id)

puts "there are #{Museum.count} row on museums"
puts 'seed done'
