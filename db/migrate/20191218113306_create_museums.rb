class CreateMuseums < ActiveRecord::Migration[6.0]
  def change
    create_table :museums do |t|
      t.references :category, null: false, foreign_key: true
      t.string :label
      t.string :name
      t.text :detail
      t.string :address
      t.string :phone_number
      t.decimal :lat
      t.decimal :lng
      t.text :open_hour

      t.timestamps
    end
  end
end
