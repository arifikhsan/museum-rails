class CreateMuseumImages < ActiveRecord::Migration[6.0]
  def change
    create_table :museum_images do |t|
      t.references :museum, null: false, foreign_key: true
      t.string :image

      t.timestamps
    end
  end
end
