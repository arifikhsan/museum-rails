class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.references :museum, null: false, foreign_key: true
      t.string :name
      t.text :detail
      t.string :image
      t.datetime :date_start
      t.datetime :date_end
      t.integer :price
      t.integer :quota

      t.timestamps
    end
  end
end
