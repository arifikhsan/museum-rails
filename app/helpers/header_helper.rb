# frozen_string_literal: true

module HeaderHelper
  class << self
    def auth
      { 'Authorization': generate }
    end

    def generate
      JWT.encode payload, Rails.application.secret_key_base, 'HS256'
    end

    def payload
      {
        user_id: User.first.id,
        time: Time.now.to_i
      }
    end
  end
end
