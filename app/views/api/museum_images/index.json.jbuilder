# frozen_string_literal: true

json.data @museum_images do |museum_image|
  json.id museum_image.id
  json.image museum_image.image_url
  json.museum_id museum_image.museum.id
  json.museum_name museum_image.museum.name
end
