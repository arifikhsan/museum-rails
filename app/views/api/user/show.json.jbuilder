# frozen_string_literal: true

json.id @user.id
json.name @user.name
json.email @user.email
json.password @user.password_digest
