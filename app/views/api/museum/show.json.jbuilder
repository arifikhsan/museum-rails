# frozen_string_literal: true

json.id @museum.id
json.category_id @museum.category.id
json.name @museum.name
json.detail @museum.detail
json.phone_number @museum.phone_number
json.open_hour @museum.open_hour
json.address @museum.address
json.label @museum.label
json.lat @museum.lat.to_f
json.lng @museum.lng.to_f
