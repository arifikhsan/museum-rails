# frozen_string_literal: true

json.data @museums do |museum|
  json.id museum.id
  json.name museum.name
end
