# frozen_string_literal: true

json.data @events do |event|
  json.id event.id
  json.name event.name
  json.detail event.detail
  json.image event.image_url
  json.date_start event.date_start
  json.date_end event.date_end
  json.price event.price
  json.full event.full
  json.quota event.quota
  json.quota_available event.quota_available
  json.museum_id event.museum.id
  json.museum_name event.museum.name
end
