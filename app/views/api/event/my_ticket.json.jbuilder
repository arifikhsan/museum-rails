# frozen_string_literal: true

json.data @event_users do |event_user|
  json.id event_user.id
  json.done event_user.event.done
  json.event do
    json.id event_user.event.id
    json.name event_user.event.name
    json.date_start event_user.event.date_start
    json.date_end event_user.event.date_end
    json.price event_user.event.price
  end
  json.museum do
    json.id event_user.event.museum.id
    json.name event_user.event.museum.name
  end
end
