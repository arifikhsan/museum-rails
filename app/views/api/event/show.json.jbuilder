# frozen_string_literal: true

json.id @event.id
json.name @event.name
json.detail @event.detail
json.image @event.image_url
json.date_start @event.date_start
json.date_end @event.date_end
json.price @event.price
json.quota @event.quota
json.full @event.full
json.quota_available @event.quota_available
json.is_registered @is_registered
json.museum_id @event.museum.id
json.museum_name @event.museum.name
json.participants @event.users
