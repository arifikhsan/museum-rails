# frozen_string_literal: true

json.data @museums do |museum|
  json.id museum.id
  json.name museum.name
  json.detail museum.detail
  json.phone_number museum.phone_number
  json.open_hour museum.open_hour
  json.category do
    json.id museum.category.id
    json.name museum.category.name
    json.color museum.category.color
  end
  json.position do
    json.lat museum.lat.to_f
    json.lng museum.lng.to_f
  end
  json.images museum.images do |image|
    json.id image.id
    json.image image.image_url
  end
  json.icon do
    json.path 'M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0'
    json.fillColor museum.category.color
    json.fillOpacity 0.7
    json.strokeWeight 0
    json.scale 1
  end
  json.label do
    json.text museum.label
    json.color 'white'
  end
end
