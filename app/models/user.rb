# frozen_string_literal: true

class User < ApplicationRecord
  has_many :event_users, dependent: :destroy
  has_many :events, through: :event_users

  has_secure_password :validations => false
  validates :email, uniqueness: true
end
