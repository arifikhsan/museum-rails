class Event < ApplicationRecord
  belongs_to :museum
  has_many :event_users, dependent: :destroy
  has_many :users, through: :event_users
  mount_uploader :image, ImageUploader

  def image_url
    return nil if image.blank?

    if Rails.env.production?
      image.url
    else
      'http://localhost:3000' + image.url
    end
  end

  def quota_available
    quota - users.count
  end

  def full
    quota == users.count
  end

  def can_register
    users.count < quota
  end

  def register id
    event_users.find_or_create_by(user_id: id)
  end

  def done
    Time.now > date_end
  end
end
