# frozen_string_literal: true

# Category
class Category < ApplicationRecord
  has_many :museums, dependent: :destroy
end
