# frozen_string_literal: true

# Museum
class Museum < ApplicationRecord
  has_many :events, dependent: :destroy
  belongs_to :category
  has_many :museum_images, dependent: :destroy
  alias_attribute :images, :museum_images
end
