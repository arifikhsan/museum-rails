class MuseumImage < ApplicationRecord
  belongs_to :museum
  mount_uploader :image, ImageUploader

  def image_url
    return nil if image.blank?

    if Rails.env.production?
      image.url
    else
      'http://localhost:3000' + image.url
    end
  end
end
