# frozen_string_literal: true

module Api
  # Event Api Controller
  class EventController < Api::AdminController
    before_action :set_event, only: %i[show update destroy register]
    skip_before_action :authorize_request, only: %i[index]

    def index
      @events = Event.all
    end

    def show
      @is_registered = @event.users.find_by(id: @current_user.id).present?
    end

    def create
      @event = Event.new(event_params)
      @event.save ? render_created : render_failed_message
    end

    def update
      @event.update(event_params) ? render_updated : render_failed_message
    end

    def destroy
      @event.destroy
      head :no_content
    end

    def register
      render_failed unless @event.can_register
      event = @event.register(@current_user.id)
      event.save ? render_success : render_failed
    end

    def my_ticket
      @event_users = @current_user.event_users
    end

    private

    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.permit(:museum_id, :name, :detail, :image, :date_start, :date_end, :price, :quota)
    end

    def register_params
      params.permit(:name, :email)
    end
  end
end
