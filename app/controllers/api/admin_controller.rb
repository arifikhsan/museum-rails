module Api
  class AdminController < Api::BaseController
    before_action :authorize_request
    before_action :authorize_admin
  end
end
