module Api
  # Auth Controller
  class AuthController < Api::BaseController
    before_action :authorize_request, only: :secret

    def login
      @user = User.find_by(email: login_params[:email].downcase)
      return render_failed if @user.nil?

      if @user.authenticate(login_params[:password])
        render json: { token: encode, is_admin: @user.is_admin }
      else
        render_failed
      end
    end

    def register
      user = User.find_by(email: register_params[:email].downcase)
      return render_registered if user.present?

      @user = User.create(register_params)
      if @user
        render json: { token: encode, is_admin: @user.is_admin }
      else
        render_failed
      end
    end

    def secret
      render json: { msg: 'you got me' }
    end

    private

    def login_params
      params.require(:user).permit(:email, :password)
    end

    def register_params
      params.require(:user).permit(:name, :email, :password)
    end

    def encode
      JWT.encode payload, Rails.application.secret_key_base
    end

    def payload
      {
        user_id: @user.id,
        time: Time.now.to_i
      }
    end
  end
end
