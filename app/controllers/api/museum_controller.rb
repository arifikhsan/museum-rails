# frozen_string_literal: true

module Api
  # Museum Api Controller
  class MuseumController < Api::AdminController
    before_action :set_museum, only: %i[show update destroy]

    def index
      @museums = Museum.all
    end

    def show; end

    def create
      @museum = Museum.new(museum_params)
      @museum.save ? render_created : render_not_created
    end

    def update
      @museum.update(museum_params) ? render_updated : render_not_updated
    end

    def destroy
      @museum.destroy
      head :no_content
    end

    private

    def set_museum
      @museum = Museum.find(params[:id])
    end

    def museum_params
      params.require(:museum).permit(:category_id, :label, :name, :detail, :address, :phone_number, :lat, :lng, :open_hour)
    end
  end
end
