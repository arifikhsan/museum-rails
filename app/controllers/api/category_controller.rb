# frozen_string_literal: true

module Api
  # Category Api Controller
  class CategoryController < Api::AdminController
    before_action :set_category, only: %i[show update destroy]

    def index
      @categories = Category.all
    end

    def show; end

    def create
      @category = Category.new(category_params)
      @category.save ? render_created : render_not_created
    end

    def update
      @category.update(category_params) ? render_updated : render_not_updated
    end

    def destroy
      @category.destroy
      head :no_content
    end

    private

    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:name, :color)
    end
  end
end
