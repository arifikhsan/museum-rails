# frozen_string_literal: true

module Api
  class BaseController < ApplicationController
    skip_before_action :verify_authenticity_token

    def authorize_request
      return raise_require_auth if auth_header.blank? || auth_header == 'null'

      decode = JWT.decode auth_header.split(' ')[1], Rails.application.secret_key_base, true, algorithm: 'HS256'
      if decode
        @current_user = User.find(decode.first['user_id'])
      else
        render_failed
      end
    end

    def authorize_admin
      # return raise_unauthorized unless @current_user.is_admin
    end

    def auth_header
      request.headers['Authorization']
    end

    def raise_not_found
      render json: { error: 'not found' }, status: :not_found
    end

    def raise_require_auth
      render json: { error: 'auth error' }, status: :unauthorized
    end

    def raise_unauthorized
      render json: { error: 'unauthorized' }, status: :unauthorized
    end

    def render_created
      render json: { message: 'created' }, status: :ok
    end

    def render_success
      render json: { message: 'success' }, status: :ok
    end

    def render_not_created
      render json: { message: 'not created' }, status: :unprocessable_entity
    end

    def render_updated
      render json: { message: 'updated' }, status: :ok
    end

    def render_not_updated
      render json: { message: 'not updated' }, status: :unprocessable_entity
    end

    def render_failed
      render json: { message: 'fail' }, status: :unprocessable_entity
    end

    def render_registered
      render json: { message: 'Email sudah terdaftar' }, status: :unprocessable_entity
    end

    def render_failed_message
      render json: {
        message: 'fail',
        errors: @user.errors.full_messages
        }, status: :unprocessable_entity
    end
  end
end
