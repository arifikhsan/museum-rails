# frozen_string_literal: true

module Api
  # Category Api Controller
  class MuseumImagesController < Api::AdminController
    before_action :set_museum_image, only: %i[show update destroy]

    def index
      @museum_images = MuseumImage.all
    end

    def show; end

    def create
      @museum_image = MuseumImage.new(museum_images_params)
      @museum_image.save ? render_created : render_not_created
    end

    def update
      @museum_image.update(museum_images_params) ? render_updated : render_not_updated
    end

    def destroy
      @museum_image.destroy
      head :no_content
    end

    private

    def set_museum_image
      @museum_image = MuseumImage.find(params[:id])
    end

    def museum_images_params
      params.permit(:museum_id, :image)
    end
  end
end
