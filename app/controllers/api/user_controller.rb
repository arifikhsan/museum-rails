# frozen_string_literal: true

module Api
  # User Api Controller
  class UserController < Api::AdminController
    before_action :set_user, only: %i[show update destroy]

    def index
      @users = User.all
    end

    def show; end

    def create
      @user = User.new(user_params)
      @user.save ? render_created : render_failed_message
    end

    def update
      @user.update(user_params) ? render_updated : render_failed_message
    end

    def destroy
      @user.destroy
      head :no_content
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
  end
end
