# frozen_string_literal: true

# Museum Controller
class MuseumController < ApplicationController
  def welcome; end

  def index
    @museums = Museum.all
  end

  def show
    @museum = Museum.find(params[:id])
  end
end
