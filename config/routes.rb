# frozen_string_literal: true

Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root 'museum#welcome'
  resources :museum, defaults: { format: :json }, only: %i[index show]
  namespace :api, defaults: { format: :json } do
    post 'login', to: 'auth#login'
    post 'register', to: 'auth#register'
    post 'secret', to: 'auth#secret'
    post 'register_event', to: 'event#register'
    get 'my_ticket', to: 'event#my_ticket'
    resources :museum, except: %i[new edit]
    resources :user, except: %i[new edit]
    resources :category, except: %i[new edit]
    resources :museum_images, except: %i[new edit]
    resources :event, except: %i[new edit]
  end
end
