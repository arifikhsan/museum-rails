module Minitest
  class HeaderSetup
    def before_setup
      super
      @request.headers['Authorization'] = HeaderHelper.generate
    end

    ActionController::TestCase.send(:include, self)
  end
end

