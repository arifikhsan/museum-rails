# frozen_string_literal: true

require 'test_helper'

module Api
  class UserControllerTest < ActionDispatch::IntegrationTest
    setup do
      @user = users(:one)
      @user_two = users(:two)
    end

    test 'index' do
      get api_user_index_url, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'show' do
      get api_user_url(@user), headers: HeaderHelper.auth
      assert_response :success
    end

    test 'update' do
      patch api_user_url(@user), params: {
        user: {
          name: 'string',
          email: 'st@ri.ng',
          password: '123'
        }
      }, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'not update when email used' do
      patch api_user_url(@user_two), params: {
        user: {
          name: 'string',
          email: @user.email,
          password: '123'
        }
      }, headers: HeaderHelper.auth
      assert_response :unprocessable_entity
    end

    test 'create' do
      assert_difference('User.count') do
        post api_user_index_url, params: {
          user: {
            name: 'string',
            email: 'st@ri.ng',
            password: '123'
          }
        }, headers: HeaderHelper.auth
      end
    end

    test 'not create when email used' do
      post api_user_index_url, params: {
        user: {
          name: 'Admin',
          email: @user.email,
          password: '123456'
        }
      }, headers: HeaderHelper.auth
      assert_response :unprocessable_entity
    end

    test 'destroy' do
      assert_difference('User.count', -1) do
        delete api_user_url(@user), headers: HeaderHelper.auth
      end
    end
  end
end
