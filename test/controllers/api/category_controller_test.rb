# frozen_string_literal: true

require 'test_helper'

module Api
  class CategoryControllerTest < ActionDispatch::IntegrationTest
    setup do
      @category = categories(:one)
    end

    test 'index' do
      get api_category_index_url, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'show' do
      get api_category_url(@category), headers: HeaderHelper.auth
      assert_response :success
    end

    test 'update' do
      patch api_category_url(@category), params: {
        category: {
          name: 'string',
          color: 'string'
        }
      }, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'create' do
      assert_difference('Category.count') do
        post api_category_index_url, params: {
          category: {
            name: 'string',
            color: 'string'
          }
        }, headers: HeaderHelper.auth
      end
    end

    test 'destroy' do
      assert_difference('Category.count', -1) do
        delete api_category_url(@category), headers: HeaderHelper.auth
      end
    end
  end
end
