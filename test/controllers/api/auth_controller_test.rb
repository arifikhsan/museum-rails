require 'test_helper'

module Api
  class AuthControllerTest < ActionDispatch::IntegrationTest
    setup do
      @user = users(:one)
    end

    test 'login' do
      post api_login_url, params: { user: { email: @user.email, password: '123456' } }
      assert_response :success
    end

    test 'should pass' do
      post api_secret_url, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'should not pass' do
      post api_secret_url
      assert_response :unauthorized
    end
  end
end
