# frozen_string_literal: true

require 'test_helper'

module Api
  class MuseumControllerTest < ActionDispatch::IntegrationTest
    setup do
      @museum = museums(:one)
    end

    test 'index' do
      get api_museum_index_url, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'show' do
      get api_museum_url(@museum), headers: HeaderHelper.auth
      assert_response :success
    end

    test 'update' do
      patch api_museum_url(@museum), params: {
        museum: {
          category_id: @museum.category.id,
          name: @museum.name,
          label: @museum.label,
          detail: @museum.detail,
          address: @museum.address,
          phone_number: @museum.phone_number,
          lat: @museum.lat,
          lng: @museum.lng,
          open_hour: @museum.open_hour
        }
      }, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'destroy' do
      assert_difference('Museum.count', -1) do
        delete api_museum_url(@museum), headers: HeaderHelper.auth
      end
    end

    test 'create' do
      assert_difference('Museum.count') do
        post api_museum_index_url, params: {
          museum: {
            category_id: @museum.category.id,
            name: 'string',
            label: 'a',
            detail: 'string',
            address: 'string',
            phone_number: '081234',
            lat: 1.1,
            lng: 2.2,
            open_hour: 'string'
          }
        }, headers: HeaderHelper.auth
      end
    end
  end
end
