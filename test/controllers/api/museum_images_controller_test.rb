# frozen_string_literal: true

require 'test_helper'

module Api
  class MuseumImagesControllerTest < ActionDispatch::IntegrationTest
    setup do
      @museum_image = museum_images(:one)
      @museum = museums(:one)
    end

    test 'index' do
      get api_museum_images_url, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'show' do
      get api_museum_image_url(@museum_image), headers: HeaderHelper.auth
      assert_response :success
    end

    test 'update' do
      patch api_museum_image_url(@museum_image),
        params: {
          museum_id: @museum.id,
          image: file_fixture('helo.jpg')
        },
        headers: {
          'Content-type': 'multipart/form-data',
          'Authorization': HeaderHelper.generate
        }
      assert_response :success
    end

    test 'create' do
      assert_difference('MuseumImage.count') do
        post api_museum_images_url,
          params: {
            museum_id: @museum.id,
            image: file_fixture('helo.jpg')
          },
          headers: {
            'Content-type': 'multipart/form-data',
            'Authorization': HeaderHelper.generate
          }
      end
    end

    test 'destroy' do
      assert_difference('MuseumImage.count', -1) do
        delete api_museum_image_url(@museum_image), headers: HeaderHelper.auth
      end
    end
  end
end
