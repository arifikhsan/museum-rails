# frozen_string_literal: true

require 'test_helper'

module Api
  class EventControllerTest < ActionDispatch::IntegrationTest
    setup do
      @event = events(:one)
      @museum = museums(:one)
      @user = users(:two)
    end

    test 'index' do
      get api_event_index_url, headers: HeaderHelper.auth
      assert_response :success
    end

    test 'show' do
      get api_event_url(@event), headers: HeaderHelper.auth
      assert_response :success
    end

    test 'update' do
      patch api_event_url(@event),
        params: {
          museum_id: @museum.id,
          name: 'aa',
          detail: 'bb',
          image: file_fixture('helo.jpg'),
          date_start: '2020-03-15 17:09:23',
          date_end: '2022-03-15 17:09:23',
          price: '12345',
        },
        headers: {
          'Content-type': 'multipart/form-data',
          'Authorization': HeaderHelper.generate
        }
      assert_response :success
    end

    test 'create' do
      assert_difference('Event.count') do
        post api_event_index_url,
          params: {
            museum_id: @museum.id,
            name: 'aa',
            detail: 'bb',
            image: file_fixture('helo.jpg'),
            date_start: '2020-03-15 17:09:23',
            date_end: '2022-03-15 17:09:23',
            price: '12345',
          },
          headers: {
            'Content-type': 'multipart/form-data',
            'Authorization': HeaderHelper.generate
          }
      end
    end

    test 'destroy' do
      assert_difference('Event.count', -1) do
        delete api_event_url(@event), headers: HeaderHelper.auth
      end
    end

    test 'register new user' do
      assert_difference('EventUser.count') do
        assert_difference('User.count') do
          post api_register_event_url,
            params: {
              id: @event.id,
              name: 'name',
              email: 'not@exists.email',
            },
            headers: HeaderHelper.auth
        end
      end
    end

    test 'register exsisting user' do
      assert_difference('EventUser.count') do
        assert_no_difference('User.count') do
          post api_register_event_url,
            params: {
              id: @event.id,
              name: @user.name,
              email: @user.email,
            },
            headers: HeaderHelper.auth
          assert_response :success
        end
      end
    end
  end
end
