# Heroku command

## set env on production

figaro heroku:set -e production

## heroku push

dpl --provider=heroku --app=museum-rails --api-key=c06af369-4726-445c-bb3f-4d8bab3cd1d9

## heroku clean

heroku restart && heroku pg:reset DATABASE --confirm museum-rails && heroku run rake db:migrate

## seed on heroku

heroku run rake db:seed

```bash
dpl --provider=heroku --app=museum-rails --api-key=c06af369-4726-445c-bb3f-4d8bab3cd1d9
heroku restart --app=museum-rails && heroku pg:reset DATABASE --confirm museum-rails --app=museum-rails && heroku run rake db:migrate --app=museum-rails 
heroku run rake db:seed --app=museum-rails

```

## rebuild database locally

```bash
rake db:drop db:create db:migrate
rake db:seed
```
